package com.demo.springbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbatchApplication {

//commented to check gitlab test
	public static void main(String[] args) {
		SpringApplication.run(SpringbatchApplication.class, args);
	}

}
